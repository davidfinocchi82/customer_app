angular.module('app', [
  'ngRoute',
  'app.customers',
  'app.services'
 ]);

angular.module('app').config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  otherwise({
    redirectTo: '/customers'
  });
}]);