angular.module('app.customers', []);

angular.module('app.customers').config(['$routeProvider', function($routeProvider) {
  var basePath = 'components/customers/'
  $routeProvider.
  when('/customers', {
    templateUrl: basePath + 'partials/customers.html',
    controller: 'CustomersCtrl',
    resolve: {
      customers: ['$http', 'NFDAPI', function ($http, NFDAPI) {
        return NFDAPI.get(basePath + 'data/customers.json', null, 'customers');
      }]
    }
  }).
  when('/customer/:itemId', {
    templateUrl: basePath + 'partials/customer.html',
    controller: 'CustomerCtrl',
    resolve: {
      customer: ['$http', '$route', 'NFDAPI', function ($http, $route, NFDAPI) {
        return $route.current.params.itemId ? NFDAPI.get(basePath + 'data/customers.json', $route.current.params.itemId, 'customers') : {};
      }]
    }
  });
}]);