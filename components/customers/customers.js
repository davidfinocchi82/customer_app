angular.module('app.customers').controller('CustomersCtrl', ['$scope', 'customers', function ($scope, customers) {
  $scope.customers = customers;
}]);

angular.module('app.customers').controller('CustomerCtrl', ['$scope', '$routeParams', 'customer', 'NFDAPI', 'custSave', function ($scope, $routeParams, customer, NFDAPI, custSave) {
  $scope.customer = customer;
  $scope.whichItem = $routeParams.itemId;

  $scope.save = function(customer, whichItem) {
    custSave(customer, whichItem);
  }

  $scope.destroy = function (whichItem) {
    NFDAPI.destroy(whichItem, 'customers').then(function(){
      window.alert("Destroyed");
      window.history.back();
    }, function(err) {
        window.alert(err);
    });
  }
}]);