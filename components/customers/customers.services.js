angular.module('app.customers').factory('custSave', ['$http', '$q', 'NFDAPI', 'validation', function($http, $q, NFDAPI, validation) {
  var type = 'customers';

  var index = JSON.parse(window.localStorage['customers']);
  var count = index.count;

  return function(customer, whichItem) {
    validation(customer).then(function() {
      var data = {
        "id": whichItem,
        "name": customer.name
      };
      if (index[whichItem]) {
        NFDAPI.patch(data, whichItem, type).then(function() {
          window.alert("Saved");
          window.history.back();
        }, function(err) {
          window.alert(err);
        });
      } else {
        NFDAPI.post(data, type).then(function() {
          window.alert("Saved");
          window.history.back();
        }, function(err) {
          window.alert(err);
        });
      }
    }, function(err) {
      window.alert(err);
    });
  }
}]);