angular.module('app.services', []);

angular.module('app.services').factory('NFDAPI', ['$http', '$q', function($http, $q) {
  return {
    get: function(path, itemId, type) {
      var deferred = $q.defer();
      if (window.localStorage[type]) {
        info = JSON.parse(window.localStorage[type]);
        if (itemId) {
          deferred.resolve(info[itemId]);
          return deferred.promise;
        } else {
          deferred.resolve(info);
          return deferred.promise;
        }
      } else {
        $http.get(path).then(function(data) {
          deferred.resolve(data.data);
          window.localStorage[type] = angular.toJson(data.data);
        }, function(err) {
          deferred.reject(err);
        });
        if (itemId) {
          return deferred.promise;
        } else {
          return deferred.promise;
        }
      }
    },
    post: function(data, type) {
      var deferred = $q.defer();
      var info = JSON.parse(window.localStorage[type]);
      info.push(data);
      localStorage.setItem(type, JSON.stringify(info));
      deferred.resolve(info);
      return deferred.promise;
    },
    patch: function(data, index, type) {
      var deferred = $q.defer();
      var info = JSON.parse(window.localStorage[type]);
      info[index] = data;
      localStorage.setItem(type, JSON.stringify(info));
      deferred.resolve(info);
      return deferred.promise;
    },
    destroy: function(index, type) {
      var deferred = $q.defer();
      var data = JSON.parse(window.localStorage[type]);
      data.splice(index, 1);
      localStorage.setItem(type, JSON.stringify(data));
      deferred.resolve(data);
      return deferred.promise;
    } 
  }
}]);

angular.module('app.services').factory('validation', ['$q', function($q) {
  return function validation(data) {
    var deferred = $q.defer();
    if (data) {
      deferred.resolve(data);
    } else {
      deferred.reject(data);
    };
    return deferred.promise;
  }
}]);